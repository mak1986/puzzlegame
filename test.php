<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
	<link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap-theme.min.css">
	<link rel="stylesheet" type="text/css" href="style.css"/>
	
	<script src="bower_components/jquery/dist/jquery.min.js"></script>
	<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
	<script src="dump.js" type="text/javascript"></script>

	<script src="js/Models/Manager.js" type="text/javascript"></script>
	<script src="js/Models/Box.js" type="text/javascript"></script>
	<script src="js/Models/Timer.js" type="text/javascript"></script>
	
	
</head>
<body ng-app="PuzzleNumber" ng-controller="gameCtrl" ng-init="init()" ng-keydown="moveByKeyEvent($event)">
	<div id="wrapper" class="container">
		<div ng-controller="authCtrl" id="content" class="row">
			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
				<div id="left-container-outer">
					<div id="left-container-inner">
					</div>
				</div>
				<div class="fb-like" data-href="https://developers.facebook.com/docs/plugins/" data-layout="standard" data-action="like" data-show-faces="true" data-share="true"></div>
				<div class="fb-comments" data-href="http://developers.facebook.com/docs/plugins/comments/" data-numposts="5"></div>
				
			</div>
			<div id="right-container-outer" class="col-md-push-1 col-lg-push-1 col-xs-12 col-sm-12 col-md-5 col-lg-5">
				<div id="right-container-inner">
					<div>
						<div class="alert alert-info" data-ng-show="salutation">Hello, {{user}}</div>
						<div class="alert alert-warning" data-ng-show="byebye">Bye bye :'(</div>
					</div>

					<button type="button" class="btn btn-primary btn-large" data-ng-show="!logged" data-ng-disabled="!facebookReady" data-ng-click="IntentLogin()">Login with Facebook</button>
					<button type="button" class="btn btn-danger btn-large" data-ng-show="logged" data-ng-disabled="!facebookReady" data-ng-click="logout()">Logout</button>

					<div id="timer"></div>
					<div>

						<button ng-click="changeMode(3)" class="btn btn-warning btn-lg">3x3</button>
						<button ng-click="changeMode(4)" class="btn btn-success btn-lg">4x4</button>
						<button ng-click="changeMode(5)" class="btn btn-danger btn-lg">5x5</button>
						<button ng-class="{'disabled':disableBtn}" ng-show="playBtnVisible" ng-click="play()" class="btn btn-default btn-lg">
							<span ng-if="disableBtn" class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span>
							PLAY <span class="glyphicon glyphicon-play"></span>
						</button>
						<button ng-class="{'disabled':disableBtn}" ng-hide="playBtnVisible" ng-click="reset()"class="btn btn-danger btn-lg">RESTART</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script src="bower_components/angular/angular.min.js" type="text/javascript"></script>
	<script src="bower_components/angular-facebook/lib/angular-facebook.js" type="text/javascript"></script>
	
	<!--App-->
	<script src="js/app.js" type='text/javascript' ></script>
	<!--Controller-->
	<script src="js/Controllers/authCtrl.js" type="text/javascript"></script>
	<script src="js/Controllers/gameCtrl.js" type="text/javascript"></script>


</body>
</html>