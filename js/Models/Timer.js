function Timer(id){
	this.id = id;
	this.centisecs = 0;
	this.secs = 0;
	this.mins = 0;
	
	this.output = function(){
		$(this.id).html(("0" + this.mins).slice(-2)+":"+("0" + this.secs).slice(-2)+':'+("0" + this.centisecs).slice(-2));	
	};
	this.refresh = function() {
		this.centisecs++;
		if(this.centisecs==100){
			this.centisecs = 0;
			this.secs++;
		}
		if(this.secs==60){
			this.secs = 0;
			this.mins++;
		}
		this.output();
		return true;
	};
	this.reset = function(){
		this.centisecs = 0;
		this.secs = 0;
		this.mins = 0;
		this.stopTimer();
		this.stopBlink();
		this.output();
		return true;
	};
	this.startTimer = function(){
		var timer = this;
		this.timerInterval = setInterval(function(){timer.refresh();}, 10);
	};
	this.stopTimer = function(){
		if(this.timerInterval!==undefined){
			clearInterval(this.timerInterval);
		}
	};
	this.startBlink = function(){
		var timer = this;
		this.blinkInterval = setInterval(function(){
			var visibility = $('#timer').css('visibility')=='visible' ? 'hidden' : 'visible';
			$('#timer').css('visibility',visibility);
		}, 300);
	};
	this.stopBlink = function(){
		if(this.blinkInterval!==undefined){
			clearInterval(this.blinkInterval);
			$('#timer').css('visibility','visible');
		}
	};
	this.showResult = function(){
		this.stopTimer();
		this.startBlink();
	};
}