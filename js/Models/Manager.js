function Manager(gameSize,colXRowCount) {
	this.colXRowCount = colXRowCount;
	this.gameSize = gameSize;
	this.boxSize = gameSize/colXRowCount;
	this.playingState = false;

	this.animationSpeed = 20;
	this.init = function(){
		this.createBoxes();
		//console.log(this.boxes);
	};
	this.start = function(){
		this.shuffleBoxes();
		this.animationSpeed = 400;
	};
	this.createBoxes = function(){
		var id = 1;
		var box;
		this.boxes = [];
		this.boxSize = this.gameSize/this.colXRowCount;
		for(var row=1;row<this.colXRowCount+1;row++){
			for(var col=1;col<this.colXRowCount+1;col++){
				box = new Box(id++, row, col, this.boxSize);
				box.init();
				this.boxes.push(box);
			}
		}
		
		// Set last box to be blank.
		this.boxes[(this.colXRowCount*this.colXRowCount)-1].setBlank();
	};
	this.moveBlankLeft= function(){
		var blankBox = this.getBlankBox();
		var box = this.findLeftBox(blankBox);
		if(box!==false){
			this.swapPosition(blankBox, box);
		}
	};
	this.moveBlankRight= function(){
		var blankBox = this.getBlankBox();
		var box = this.findRightBox(blankBox);
		if(box!==false){
			this.swapPosition(blankBox, box);
		}
	};
	this.moveBlankUp= function(){
		var blankBox = this.getBlankBox();
		var box = this.findOverBox(blankBox);
		if(box!==false){
			this.swapPosition(blankBox, box);
		}
	};
	this.moveBlankDown= function(){
		var blankBox = this.getBlankBox();
		var box = this.findUnderBox(blankBox);
		if(box!==false){
			this.swapPosition(blankBox, box);
		}
	};
	this.findLeftBox = function(box){
		var col = box.getCol();
		if(col>1){
			return this.getBoxByPosition(box.getRow(), box.getCol()-1);
		}
		return false;
	};
	this.findRightBox = function(box){
		var col = box.getCol();
		if(col < this.colXRowCount){
			return this.getBoxByPosition(box.getRow(), box.getCol()+1);
		}
		return false;
	};
	this.findOverBox = function(box){
		var row = box.getRow();
		if(row>1){
			return this.getBoxByPosition(box.getRow()-1, box.getCol());
		}
		return false;
	};
	this.findUnderBox = function(box){
		var row = box.getRow();
		if(row < this.colXRowCount){
			return this.getBoxByPosition(box.getRow()+1, box.getCol());
		}
		return false;
	};
	this.getBoxByPosition = function(row, col){
		var length = this.boxes.length;
		for(var i = 0; i < length; i++){
			if(this.boxes[i].getRow()==row && this.boxes[i].getCol()==col){
				return this.boxes[i];
			}
		}
	};
	this.swapPosition = function(blankBox, box){
		var blankBoxRow = blankBox.getRow();
		var blankBoxCol = blankBox.getCol();
		var boxRow = box.getRow();
		var boxCol = box.getCol();
		blankBox.setRow(boxRow, this.animationSpeed);
		blankBox.setCol(boxCol, this.animationSpeed);
		box.setRow(blankBoxRow, this.animationSpeed);
		box.setCol(blankBoxCol, this.animationSpeed);
	};
	this.getBlankBox = function(){
		var length = this.boxes.length;
		for(var i = 0; i < length; i++){
			if(this.boxes[i].isBlank()){
				return this.boxes[i];
			}
		}
	};

	//show output in console
	this.output = function(){
		var str = "";
		for(var row=1;row<this.colXRowCount+1;row++){
			for(var col=1;col<this.colXRowCount+1;col++){
				str += this.getBoxByPosition(row, col).getId()+", ";
			}
			console.log(str);
			str = "";
		}
	};

	this.checkWin = function(){
		if(this.playingState){
			increment = 1;
			for(var row=1;row<this.colXRowCount+1;row++){
				for(var col=1;col<this.colXRowCount+1;col++){
					if(this.getBoxByPosition(row, col).getId()!=increment){
						return false;
					}
					increment++;
				}
			}
			/*		console.log('win');*/
			return true;
		}
	};
	this.setPlayingState = function(state){
		this.playingState = state;
	};
	this.isPlaying = function(){
		return this.playingState;
	};
	this.shuffleBoxes = function(){
		this.animationSpeed = 20;
		for(var i = 1; i<=500; i++){
			var rand = Math.floor((Math.random() * 4) + 1);
			console.log(rand);
			if(rand===1){
				this.moveBlankUp();
			}else if(rand===2){
				this.moveBlankRight();
			}else if(rand===3){
				this.moveBlankDown();
			}else{
				this.moveBlankLeft();
			}
		}
	};
	this.resize = function(gameSize){
		this.gameSize = gameSize;
		this.boxSize = this.gameSize/this.colXRowCount;
		var length = this.boxes.length;
		for(var i = 0; i < length; i++){
			this.boxes[i].resize(this.boxSize);
		}
	};
	this.reset = function(){
		this.setPlayingState(false);
/*		console.log('clicked');*/
		$('#left-container-inner').html("");
		this.createBoxes();
	};
	this.setColXRowCount = function(colXRowCount){
		this.colXRowCount = colXRowCount;
	};
	this.getBoxSize = function(){
		return this.boxSize;
	};
}