function Box(id, row, col, size){
	this.id = id;
	this.row = row;
	this.col = col;
	this.size = size;
	this.blank = false;
	this.generateColor = function(){
		var colors = ['#FEF400','#C6D900','#EAAE01','#0D9D31','#FD8300','#F8272B','#FC271E','#AB2870','#48277E','#002A8B','#0063CB','#007F96'];
		var count = this.row*this.col;
		this.colors = [];
		for(var i=0; i<count;i++){
			this.colors.push(colors[i%12]);
		}
	};
	this.init = function(){
		this.generateColor();
		$('#left-container-inner').append("<div id='box-"+this.id+"' class='block'><p>"+this.id+"</p></div>");
		$('#box-'+this.id).css({
			'top': (row-1)*this.size+'px',
			'left': (col-1)*this.size+'px',
			'width':this.size-2,
			'height':this.size-2,
			/*'background-color': this.colors.pop(),*/
			'background-color': '#f7f7f7',
			'border':'1px solid #efefef',
			'border-radius': '4px',
			'-webkit-border-radius':'4px',
			'-moz-border-radius':'4px',
			'color':'#000',
			'font-family':'impact',
			'font-size':this.size/1.5+'px'
		});
	};
	this.isBlank = function(){
		return this.blank;
	};
	this.setBlank = function(){
		this.blank = true;
		$('#box-'+this.id).css({'display':'none'});
	};
	this.setRow = function(row, animationSpeed){
		if(!this.isBlank()){
			//going up
			if(this.row > row){
				$("#box-"+this.id).animate({"top":"-="+this.size+"px"}, animationSpeed);
			}
			//going down
			else if(this.row < row){
				$("#box-"+this.id).animate({"top":"+="+this.size+"px"}, animationSpeed);
			}
		}
		
		this.row = row;
	};
	this.setCol = function(col, animationSpeed){
		if(!this.isBlank()){
			//going up
			if(this.col > col){
				$("#box-"+this.id).animate({"left":"-="+this.size+"px"}, animationSpeed);
			}
			//going down
			else if(this.col < col){
				$("#box-"+this.id).animate({"left":"+="+this.size+"px"}, animationSpeed);
			}
		}
		this.col = col;
	};
	this.getRow = function(){
		return this.row;
	};
	this.getCol = function(){
		return this.col;
	};
	this.getId = function(){
		return this.id;
	};
	this.resize = function(size){
		console.log('resizing:'+size);
		this.size=size;
		$('#box-'+this.id).css({
			'top': (this.row-1)*this.size+'px',
			'left': (this.col-1)*this.size+'px',
			'width':this.size-2,
			'height':this.size-2,
			'font-size':this.size/1.5+'px'
		});
	};
}