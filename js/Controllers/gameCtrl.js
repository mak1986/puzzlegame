app.controller('gameCtrl', function ($scope, $http) {
	$scope.disableBtn = false;
	$scope.setDisableBtn = function(bool){
		$scope.disableBtn = bool;
	};
	$scope.playBtnVisible = true;
	$scope.setPlayBtnVisible = function(bool){
		$scope.playBtnVisible = bool;
	};
	$scope.init = function(){
		$scope.preventBrowserAction();
		$scope.initManager();
		$scope.initTimer();
		$scope.watchResize();
	};
	$scope.initManager = function(){
		var screenHeight = $(window).height();
		var width = $('#left-container-inner').width();

		if(screenHeight<width){
			$('#left-container-inner').css('height',(screenHeight-50)+'px');
			$scope.manager = new Manager(screenHeight-50 , 3);
		}else{
			$('#left-container-inner').css('height',width+'px');
			$scope.manager = new Manager(width , 3);
		}

		$scope.manager.init();
	};
	$scope.changeMode = function(mode){
		$scope.manager.setColXRowCount(mode);
		$scope.reset();
	};
	$scope.initTimer = function(){
		$scope.timer = new Timer('#timer');
		$scope.timer.output();
		$('#timer').css('font-size',($scope.manager.getBoxSize()/1.5)+'px');
	};
	$scope.play = function(){
		if(!$scope.disableBtn){
			$scope.manager.start();
			$scope.timer.reset();
			$scope.setDisableBtn(true);
			// Wait 2 secs and then start the timer
			setTimeout(function(){
				$scope.$apply(function(){
					$scope.manager.setPlayingState(true);
					$scope.timer.startTimer();
					$scope.setPlayBtnVisible(false);
					$scope.setDisableBtn(false);
				});
			}, 2000);
		}
	};
	$scope.reset = function(){
		$scope.timer.reset();
		$scope.manager.reset();
		$scope.setPlayBtnVisible(true);
	};
	$scope.moveByKeyEvent = function($event){
		if($scope.manager.isPlaying()){
			//right key pressed
			if ($event.keyCode== 39) {
				$scope.manager.moveBlankLeft();
			}
			//left key pressed
			if ($event.keyCode == 37) {
				$scope.manager.moveBlankRight();
			}
			//down key pressed
			if ($event.keyCode == 40) {
				$scope.manager.moveBlankUp();
			}
			//up key pressed
			if ($event.keyCode == 38) {
				$scope.manager.moveBlankDown();
			}
			if($scope.manager.checkWin()){
				$scope.manager.setPlayingState(false);
				$scope.timer.showResult();
				$scope.setPlayBtnVisible(true);
			}
		}
		console.log($scope.playBtnVisible);
	};
	$scope.watchResize = function(){
		window.onresize = function(event) {
			$scope.scaleBoard();
		};
	};
	$scope.preventBrowserAction = function(){
		window.addEventListener("keydown", function(e) {
			// space and arrow keys
			if([37, 38, 39, 40].indexOf(e.keyCode) > -1) {
				e.preventDefault();
			}
		}, false);
	};
	$scope.scaleBoard = function(){
		var screenHeight = $(window).height();
		var width = $('#left-container-inner').width();

		//console.log(screenHeight+" "+width);
		
		if(screenHeight<width){
			$('#left-container-inner').css('height',(screenHeight-50)+'px');
			$scope.manager.resize(screenHeight-50);
		}else{
			$('#left-container-inner').css('height',width+'px');
			$scope.manager.resize(width);
		}
		$('#timer').css('font-size',($scope.manager.getBoxSize()/1.5)+'px');
	};

});